import {rest_server_url} from 'app.config';
import db_collection_names from 'resources/db-collection-names';
import {extractId} from 'zenbaei_js_lib/src/utils/http-utils';
import MongoUtils from 'zenbaei_js_lib/src/utils/mongo-utils';
import ObjectUtils from 'zenbaei_js_lib/src/utils/object-utils';
import User, {Sport} from './user';

const mongoUtils: MongoUtils<User> = new MongoUtils<User>(
  rest_server_url,
  db_collection_names.users,
);

const objectUtils: ObjectUtils<User> = new ObjectUtils(User);

/**
 * Returns created document id.
 */
export const save = async (user: User): Promise<string> => {
  user.email = user.email.toLowerCase();
  const response: Response = await mongoUtils.save(user);
  return extractId(response);
};

export const update = async (user: User): Promise<User> => {
  await mongoUtils.update(user);
  return user;
};

export const getById = async (id: string): Promise<User> => {
  const response: Response = await mongoUtils.getById(id);
  const body: Object = await response.json();
  return objectUtils.toTypedObject(body);
};

export const getByEmail = async (email: string): Promise<User> => {
  const response: Response = await mongoUtils.getByQueryObject({
    email: email.toLowerCase(),
  });
  const body: Object = await response.json();
  return objectUtils.toTypedObject(body);
};

export const getBySportsCityCountry = async ({
  sports,
  city,
  country,
}: {
  sports: Sport[];
  city: string;
  country: string;
}): Promise<User[]> => {
  const sportsQuery: object[] = sports.map((spr) => {
    return {name: spr.name};
  });
  const queryObject = {
    country: country,
    city: city,
    sports: MongoUtils.orQuery(sportsQuery),
  };
  const response: Response = await mongoUtils.getByQueryObject(queryObject);
  const body: Object[] = await response.json();
  return objectUtils.toTypedObjects(body);
};

export const isValidPassword = (
  password: string,
  encodedPass: string,
): boolean => {
  return password === encodedPass ? true : false;
};
