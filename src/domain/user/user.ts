import {_id} from 'zenbaei_js_lib/src/utils/mongo-utils';

export default class User extends _id {
  email!: string;
  password!: string;
  nickname!: string;
  country: string = '';
  city: string = '';
  age: number = 30;
  gender: string = 'M';
  notificationToken!: string;
  sports: Sport[] = [];
}

export class Sport {
  name: string;
  proficiency: Proficiency;

  constructor(name: string, proficiency: Proficiency) {
    this.name = name;
    this.proficiency = proficiency;
  }
}

export enum Proficiency {
  BEGINNER = 0,
  INTERMEDIATE = 1,
  EXPERT = 2,
}

export const mapProficiencyToString = (proficiency: Proficiency): string => {
  switch (proficiency) {
    case Proficiency.BEGINNER:
      return 'beginner';
    case Proficiency.INTERMEDIATE:
      return 'intermediate';
    default:
      return 'expert';
  }
};
