import {_id} from 'zenbaei_js_lib/src/utils/mongo-utils';

export default class Country extends _id {
  country!: string;
  cities!: string[];
}
