import {rest_server_url} from 'app.config';
import db_collection_names from 'resources/db-collection-names';
import MongoUtils from 'zenbaei_js_lib/src/utils/mongo-utils';
import ObjectUtils from 'zenbaei_js_lib/src/utils/object-utils';
import Country from './country';

const mongoUtils: MongoUtils<Country> = new MongoUtils<Country>(
  rest_server_url,
  db_collection_names.countries,
);

const objectUtils: ObjectUtils<Country> = new ObjectUtils(Country);

export const getByCountry = async (country: string): Promise<Country> => {
  const response: Response = await mongoUtils.getByQueryObject({
    country: country,
  });
  const body: Object = await response.json();
  return objectUtils.toTypedObject(body);
};

//TODO: modify rest api to allow projection
export const getCountries = async (): Promise<Country[]> => {
  const response: Response = await mongoUtils.getAll();
  const body: Object[] = await response.json();
  return objectUtils.toTypedObjects(body);
};
