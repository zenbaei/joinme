import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import Styles from 'resources/css-styles';
import {NavigationScreens} from 'resources/navigation-screens';
import {update} from 'src/domain/user/user-service';
import validate from 'validate.js';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Errors from 'zenbaei_js_lib/src/react/components/errors';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import InputText from 'zenbaei_js_lib/src/react/components/input-text';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import {hideBackButton} from 'zenbaei_js_lib/src/react/utils/react-utils';

export default function NicknameSetupScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'nicknameSetupScreen'>) {
  const {user} = route.params;
  const [nicknameState, setNicknameState] = useState('');
  const [messagesState, setMessagesState] = useState(['']);

  useFocusEffect(useCallback(() => setMessagesState(['']), []));

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useFocusEffect(useCallback(() => hideBackButton(navigation), []));

  const onNicknameChange = (val: string) => {
    setNicknameState(val);
  };

  const onNextPress = () => {
    const msg: string[] = validate({nickname: nicknameState}, constraints, {
      format: 'flat',
    });

    if (msg && msg.length > 0) {
      setMessagesState(msg);
      return;
    }

    user.nickname = nicknameState;
    update(user).then(() =>
      navigation.navigate('locationSetupScreen', {user: user}),
    );
  };

  return (
    <Grid>
      <Col id="col1">
        <Card>
          <Text text="Pick A Name" />
          <InputText
            placeholder="nickname"
            value={nicknameState}
            onChangeFunc={onNicknameChange}
          />
          <Errors messages={messagesState} style={Styles.errorText} />
        </Card>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextPress} />
      </Col>
    </Grid>
  );
}

const constraints = {
  nickname: {
    presence: {
      allowEmpty: false,
      message: 'is required',
    },
  },
};
