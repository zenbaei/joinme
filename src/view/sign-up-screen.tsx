import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import {NavigationScreens} from 'resources/navigation-screens';
import User from 'src/domain/user/user';
import {getByEmail, save, update} from 'src/domain/user/user-service';
import validate from 'validate.js';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Errors from 'zenbaei_js_lib/src/react/components/errors';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import InputText from 'zenbaei_js_lib/src/react/components/input-text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import {detectUserLocation} from 'zenbaei_js_lib/src/utils/geolocation-utils';
import {acquireDeviceToken} from 'zenbaei_js_lib/src/utils/messaging-utils';

export default function SignUpScreen({
  navigation,
}: NavigationProps<NavigationScreens, 'signUpScreen'>) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [messageState, setMessagesState] = useState(['']);
  const user: User = new User();

  // cleaning up
  useFocusEffect(useCallback(() => setMessagesState(['']), []));

  const onEmailChange = (val: string) => {
    setEmail(val);
  };

  const onPasswordChange = (val: string) => {
    setPassword(val);
  };

  const onPasswordConfirmChange = (val: string) => {
    setPasswordConfirm(val);
  };

  const onSignUp = async () => {
    const isValid: boolean = await _validate();

    if (!isValid) {
      return;
    }

    user.email = email;
    user.password = password;
    save(user).then((id) => {
      user._id = id;
      initUserInfo();
      navigation.navigate('nicknameSetupScreen', {
        user: user,
      });
    });
  };

  const initUserInfo = () => {
    detectUserLocation().then((location) => {
      user.city = location.city;
      user.country = location.country;
    });
    acquireDeviceToken((token) => {
      user.notificationToken = token;
      update(user);
    });
  };

  const _validate = async () => {
    const msgs: string[] = validate(
      {email: email, password: password, passwordConfirmation: passwordConfirm},
      constraints,
      {
        format: 'flat',
      },
    );

    if (validate.isDefined(msgs) && msgs.length > 0) {
      setMessagesState(msgs);
      return false;
    }

    const usr: User = await getByEmail(email);

    if (validate.isDefined(usr.email)) {
      setMessagesState(['Email already exists.']);
      return false;
    }

    return true;
  };

  return (
    <Grid>
      <Col id="col1" proportion={1}>
        <Card>
          <InputText
            placeholder="email"
            value={email}
            onChangeFunc={onEmailChange}
          />
          <InputText
            placeholder="password"
            value={password}
            password={true}
            onChangeFunc={onPasswordChange}
          />
          <InputText
            placeholder="confirm password"
            value={passwordConfirm}
            password={true}
            onChangeFunc={onPasswordConfirmChange}
          />
          <Errors messages={messageState} />
        </Card>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Sign up" onPressFunc={onSignUp} />
      </Col>
    </Grid>
  );
}

const constraints = {
  email: {
    email: {
      message: 'is not valid',
    },
  },
  password: {
    presence: {
      allowEmpty: false,
      message: 'is required',
    },
  },
  passwordConfirmation: {
    equality: 'password',
  },
};
