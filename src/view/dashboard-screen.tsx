import React, {useCallback, useEffect, useState} from 'react';
import {Alert, ScrollView} from 'react-native';
import {NavigationScreens} from 'resources/navigation-screens';
import User, {mapProficiencyToString, Sport} from 'src/domain/user/user';
import {getBySportsCityCountry} from 'src/domain/user/user-service';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Data, {RecordArgs} from 'zenbaei_js_lib/src/react/components/data';
import Grid, {Col, Row} from 'zenbaei_js_lib/src/react/components/grid';
import Link from 'zenbaei_js_lib/src/react/components/link';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import Tuple from 'zenbaei_js_lib/src/types/tuple';
import {
  listenForNotifications,
  pushNotification,
} from 'zenbaei_js_lib/src/utils/messaging-utils';

export default function DashboardScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'dashboardScreen'>) {
  const {user} = route.params;
  const initialVal: Tuple<User, boolean>[] = [];
  const [guestsState, setGuestsState] = useState(initialVal);

  listenForNotifications();

  /**
   * Other users with same sports.
   */
  const initGuestsState = () => {
    const arr: Tuple<User, boolean>[] = [];
    getBySportsCityCountry(user).then((guests) => {
      guests.forEach((gst) => {
        // exclude the current user
        if (gst.email !== user.email) {
          gst.sports = filterGuestSports(gst.sports, user.sports);
          arr.push(new Tuple(gst, false));
        }
      });
      setGuestsState(arr);
    });
  };

  useEffect(useCallback(initGuestsState, []), []);

  const onInvitePress = useCallback(
    (index: number) => {
      let arr: Tuple<User, boolean>[] = [...guestsState];
      const gstStt = arr[index];
      gstStt.value = true;
      pushNotification(
        gstStt.key.notificationToken,
        'Invitation',
        'Hi there',
      ).then(
        (response) => {
          Alert.alert(`response status: ${JSON.stringify(response.status)}`);
        },
        (reason) => {
          Alert.alert(reason);
        },
      );
      setGuestsState(arr);
    },
    [guestsState],
  );

  return (
    <Grid>
      <Col id={'container_col'}>
        <ScrollView>
          <Card>
            <Data
              data={guestsState}
              renderRecord={(gstStt, index) => {
                return (
                  <Record
                    element={gstStt}
                    index={index}
                    onEventHandler={onInvitePress}
                  />
                );
              }}
            />
          </Card>
        </ScrollView>
      </Col>
    </Grid>
  );
}
const Record = ({
  element,
  onEventHandler,
  index,
}: RecordArgs<Tuple<User, boolean>>): JSX.Element => {
  const {email} = element.key;
  return (
    <Row id={email}>
      <Col id={email + index} proportion={1}>
        <Text text={element.key.nickname} />
      </Col>
      <Col id={email + index + 1} proportion={2}>
        {element.key.sports.map((spt) => {
          return (
            <Row id={email}>
              <Col id={email + index}>
                <Text text={spt.name} />
              </Col>
              <Col id={email + index + 1}>
                <Text text={mapProficiencyToString(spt.proficiency)} />
              </Col>
            </Row>
          );
        })}
      </Col>
      <Col id={email + index + 2} proportion={0}>
        <Link
          label="invite"
          isPressed={element.value}
          onPressFunc={onEventHandler}
          index={index}
        />
      </Col>
    </Row>
  );
};

const filterGuestSports = (
  guestSports: Sport[],
  userSports: Sport[],
): Sport[] => {
  let filteredGstSpts: Sport[] = [];
  guestSports.forEach((gstSpt) => {
    if (inUserSports(gstSpt, userSports)) {
      filteredGstSpts.push(gstSpt);
    }
  });
  return filteredGstSpts;
};

const inUserSports = (
  guestSport: Sport,
  userSports: Sport[],
): Sport | undefined => {
  return userSports.find((usrSpt) => usrSpt.name === guestSport.name);
};
