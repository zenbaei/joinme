import React, {useState} from 'react';
import {RadioButton} from 'react-native-paper';
import {NavigationScreens} from 'resources/navigation-screens';
import {update} from 'src/domain/user/user-service';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Grid, {Col, Row} from 'zenbaei_js_lib/src/react/components/grid';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';

export default function GenderSetupScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'genderSetupScreen'>) {
  const {user} = route.params;
  const [genderState, setGenderState] = useState('M');

  const onNextButtonPress = () => {
    user.gender = genderState;
    update(user).then(() =>
      navigation.navigate('sportSetupScreen', {
        user: user,
      }),
    );
  };

  return (
    <Grid>
      <Col id="col1">
        <Card>
          <Text text="Your Gender" />
          <Row id="row1">
            <Text text="Male" />
            <RadioButton
              value="first"
              status={genderState === 'M' ? 'checked' : 'unchecked'}
              onPress={() => {
                setGenderState('M');
              }}
            />
          </Row>
          <Row id="row2">
            <Text text="Female" />
            <RadioButton
              value="second"
              status={genderState === 'F' ? 'checked' : 'unchecked'}
              onPress={() => {
                setGenderState('F');
              }}
            />
          </Row>
        </Card>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextButtonPress} />
      </Col>
    </Grid>
  );
}
