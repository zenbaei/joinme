import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import {ScrollView} from 'react-native';
import {NavigationScreens} from 'resources/navigation-screens';
import {Proficiency, Sport} from 'src/domain/user/user';
import {update} from 'src/domain/user/user-service';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import DataGrid from 'zenbaei_js_lib/src/react/components/data-grid';
import Errors from 'zenbaei_js_lib/src/react/components/errors';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import IconButton from 'zenbaei_js_lib/src/react/components/icon-button';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import Tuple from 'zenbaei_js_lib/src/types/tuple';

export default function SportsSetupScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'sportSetupScreen'>) {
  const {user} = route.params;
  const [sportsState, setSportsState] = useState(initialSportsState());
  const [messagesState, setMessagesState] = useState(['']);

  useFocusEffect(useCallback(() => setMessagesState(['']), []));

  const onIconPress = useCallback((index: number) => {
    let arr: Tuple<string, boolean>[] = [...sportsState];
    const sportStat = arr[index];
    sportStat.value = !sportStat.value;
    setSportsState(arr);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onNextBtnPress = useCallback(() => {
    const arr: Sport[] = [];
    sportsState.forEach(({key, value}) => {
      if (value) {
        arr.push(new Sport(key, Proficiency.INTERMEDIATE));
      }
    });
    if (arr.length === 0) {
      setMessagesState(['Select at least one sport']);
      return;
    }
    user.sports = arr;
    update(user).then(() =>
      navigation.navigate('proficiencySetupScreen', {user: user}),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Grid key={'outerGrid'}>
      <Col id="col1" vertical="flex-start">
        <ScrollView>
          <Card>
            <Text text="Select your sports:" />
            <DataGrid
              key="outerData"
              recordPerRow={4}
              data={sportsState}
              renderRecord={(sprStt, index) => {
                return (
                  <IconButton
                    iconName={sprStt.key}
                    isSelected={sprStt.value}
                    index={index}
                    onEventHandler={onIconPress}
                  />
                );
              }}
            />
            <Errors messages={messagesState} />
          </Card>
        </ScrollView>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextBtnPress} />
      </Col>
    </Grid>
  );
}

const initialSportsState = (): Tuple<string, boolean>[] => {
  return [
    'football-ball',
    'baseball-ball',
    'basketball-ball',
    'running',
    'skiing',
  ].map((name) => {
    return new Tuple(name, false);
  });
};
