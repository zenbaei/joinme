import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import {NavigationScreens} from 'resources/navigation-screens';
import User from 'src/domain/user/user';
import validate from 'validate.js';
import Login from 'zenbaei_js_lib/src/react/components/login';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import {getByEmail, isValidPassword} from '../domain/user/user-service';

export default function LoginScreen({
  navigation,
}: NavigationProps<NavigationScreens, 'loginScreen'>) {
  const [errorMsg, setErrorMsg] = useState('');

  // cleaning up
  useFocusEffect(useCallback(() => setErrorMsg(''), []));

  const loginClbk = async (id: string, password: string) => {
    if (idIsValid(id) && passwordIsValid(id)) {
      const user: User = await getByEmail(id);
      if (!isValidPassword(password, user.password)) {
        setErrorMsg('wrong user or password.');
        return;
      }

      navigation.navigate('dashboardScreen', {user: user});
    }
  };

  const idIsValid = (id: string): boolean => {
    if (id.length === 0 || !validate.isDefined(id)) {
      setErrorMsg('Email cannot be empty');
      return false;
    }
    return true;
  };

  const passwordIsValid = (password: string): boolean => {
    if (password.length === 0 || !validate.isDefined(password)) {
      setErrorMsg('Password is required');
      return false;
    }
    return true;
  };

  /*
  const onSignUp = () => {
    navigation.navigate('signUpScreen');
  };
*/

  return (
    <Login
      id_placeholder="Email"
      on_submit_callback={loginClbk}
      error_messages={[errorMsg]}
    />
  );
}

/*
const navigate = (
  navigation: StackNavigationProp<AppStackNavigationParams, 'loginScreen'>,
  user: User,
): void => {
  navigation.navigate('dashboardScreen', {user: user});
};
*/
