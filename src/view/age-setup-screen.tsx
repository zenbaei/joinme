import React, {useState} from 'react';
import Styles from 'resources/css-styles';
import {NavigationScreens} from 'resources/navigation-screens';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import Slider from 'zenbaei_js_lib/src/react/components/slider';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import {update} from '../domain/user/user-service';

export default function AgeSetupScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'ageSetupScreen'>) {
  const {user} = route.params;
  const [age, setAge] = useState(user.age);

  const onNextButtonPress = () => {
    user.age = age;
    update(user).then(() =>
      navigation.navigate('genderSetupScreen', {
        user: user,
      }),
    );
  };

  return (
    <Grid>
      <Col id="col1">
        <Card>
          <Text text="Your Age Group" />
          <Text text={age + " 's"} />
          <Slider
            style={Styles.ageSlider}
            step={10}
            value={age}
            minimumValue={10}
            maximumValue={90}
            onValueChangeFunc={setAge}
          />
        </Card>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextButtonPress} />
      </Col>
    </Grid>
  );
}
