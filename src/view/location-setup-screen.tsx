import {Picker} from '@react-native-community/picker';
import React, {useCallback, useEffect, useState} from 'react';
import Styles from 'resources/css-styles';
import {NavigationScreens} from 'resources/navigation-screens';
import {getByCountry, getCountries} from 'src/domain/country/country-service';
import {update} from 'src/domain/user/user-service';
import validate from 'validate.js';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';

export default function LocationSetupScreen({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'locationSetupScreen'>) {
  const {user} = route.params;
  const [selectedCountry, setSelectedCountry] = useState(user.country);
  const [selectedCity, setSelectedCity] = useState(user.city);
  const initialElements: Element[] = [];
  const [countryPikcerState, setCountryPikcerState] = useState(initialElements);
  const [cityPickerState, setCityPickerState] = useState(initialElements);

  const onCountryPickerChange = (val: string | number) => {
    setSelectedCountry(val as string);
    fillCityPickerState(val as string);
  };

  const onCityPickerChange = (val: string | number) => {
    setSelectedCity(val as string);
  };

  const onNextButtonPress = () => {
    user.country = selectedCountry;
    user.city = selectedCity;
    update(user).then(() => {
      navigation.navigate('ageSetupScreen', {user: user});
    });
  };

  useEffect(
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useCallback(() => fillCountryPickerState(), []),
    [],
  );

  const fillCountryPickerState = (): void => {
    getCountries().then((countries) => {
      const elements: Element[] = countries.map((c) => (
        <Picker.Item key={c.country} label={c.country} value={c.country} />
      ));
      setCountryPikcerState(elements);
      const slcCty: string = validate.isEmpty(selectedCountry)
        ? countries[0].country
        : selectedCountry;
      fillCityPickerState(slcCty);
    });
  };

  const fillCityPickerState = (val: string) => {
    let result: Element[] = [];
    getByCountry(val).then((country) => {
      country.cities.forEach((cty) => {
        result.push(<Picker.Item key={cty} label={cty} value={cty} />);
      });
      setCityPickerState(result);
    });
  };

  return (
    <Grid>
      <Col id="col1">
        <Card>
          <Text text="Set Your Location" />
          <Picker
            style={Styles.picker}
            selectedValue={selectedCountry}
            onValueChange={onCountryPickerChange}>
            {countryPikcerState}
          </Picker>
          <Picker
            style={Styles.picker}
            selectedValue={selectedCity}
            onValueChange={onCityPickerChange}>
            {cityPickerState}
          </Picker>
        </Card>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextButtonPress} />
      </Col>
    </Grid>
  );
}
