import {FontAwesomeIcon} from '@fortawesome//react-native-fontawesome';
import {IconName} from '@fortawesome/fontawesome-svg-core';
import React, {useCallback, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import Styles from 'resources/css-styles';
import {NavigationScreens} from 'resources/navigation-screens';
import {mapProficiencyToString, Proficiency, Sport} from 'src/domain/user/user';
import {update} from 'src/domain/user/user-service';
import Button from 'zenbaei_js_lib/src/react/components/button';
import Card from 'zenbaei_js_lib/src/react/components/card';
import {RecordArgs} from 'zenbaei_js_lib/src/react/components/data';
import DataGrid from 'zenbaei_js_lib/src/react/components/data-grid';
import Grid, {Col} from 'zenbaei_js_lib/src/react/components/grid';
import Slider from 'zenbaei_js_lib/src/react/components/slider';
import Text from 'zenbaei_js_lib/src/react/components/text';
import NavigationProps from 'zenbaei_js_lib/src/react/types/navigation-props';
import Tuple from 'zenbaei_js_lib/src/types/tuple';

export default function ProficiencySetup({
  navigation,
  route,
}: NavigationProps<NavigationScreens, 'proficiencySetupScreen'>) {
  const {user} = route.params;
  const [proficienciesState, setProficienciesState] = useState(
    initialProficienciesState(user.sports),
  );

  const onSliderValueChange = useCallback(
    (index: number, sliderVal?: number | string) => {
      proficienciesState[index].value = Number(sliderVal);
      const arr: Tuple<string, Proficiency>[] = [...proficienciesState];
      setProficienciesState(arr);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onNextBtnPress = () => {
    user.sports = proficienciesState.map(({key, value}) => {
      return new Sport(key, value);
    });
    update(user).then(() =>
      navigation.navigate('dashboardScreen', {user: user}),
    );
  };

  return (
    <Grid>
      <Col id="col1">
        <ScrollView>
          <Card>
            <Text text="Your proficiency level:" />
            <DataGrid
              recordPerRow={1}
              data={proficienciesState}
              renderRecord={(prfStt, index) => {
                return (
                  <Record
                    element={prfStt}
                    onEventHandler={onSliderValueChange}
                    index={index}
                  />
                );
              }}
            />
          </Card>
        </ScrollView>
      </Col>
      <Col id="col2" proportion={0}>
        <Button label="Next" onPressFunc={onNextBtnPress} />
      </Col>
    </Grid>
  );
}

const Record = ({
  element,
  onEventHandler,
  index,
}: RecordArgs<Tuple<string, Proficiency>>) => {
  return (
    <>
      <FontAwesomeIcon icon={element.key as IconName} />
      <Text text={element.key.replace('-ball', '')} />
      <View style={styles.sliderContainer}>
        <Text text={mapProficiencyToString(element.value)} />
        <Slider
          style={Styles.proficiencySlider}
          minimumValue={0}
          maximumValue={2}
          step={1}
          value={element.value}
          onValueChangeFunc={(sliderVal: string) =>
            onEventHandler(index, sliderVal)
          }
        />
      </View>
    </>
  );
};

const initialProficienciesState = (
  sports: Sport[],
): Tuple<string, Proficiency>[] => {
  return sports.map(({name, proficiency}) => {
    return new Tuple(name, proficiency);
  });
};

const styles = StyleSheet.create({
  sliderContainer: {
    alignSelf: 'stretch',
  },
});
