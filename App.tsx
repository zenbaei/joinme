import {library} from '@fortawesome/fontawesome-svg-core';
import {
  faArrowLeft,
  faBaseballBall,
  faBasketballBall,
  faFootballBall,
  faRunning,
  faSkiing,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {StatusBar} from 'react-native';
import appColors from 'resources/app-colors';
import Styles from 'resources/css-styles';
import {NavigationScreens} from 'resources/navigation-screens';
import NicknameSetupScreen from 'src/view/nickman-setup-screen';
import AgeSetupScreen from './src/view/age-setup-screen';
import DashboardScreen from './src/view/dashboard-screen';
import GenderSetupScreen from './src/view/gender-setup-screen';
import LocationSetupScreen from './src/view/location-setup-screen';
import LoginScreen from './src/view/login-screen';
import ProficiencySetup from './src/view/proficiency-setup';
import SignUpScreen from './src/view/sign-up-screen';
import SportsSetupScreen from './src/view/sports-setup-screen';

//adding icons
library.add(
  faFootballBall,
  faBaseballBall,
  faBaseballBall,
  faBasketballBall,
  faRunning,
  faSkiing,
  faSpinner,
  faArrowLeft,
);

const Stack = createStackNavigator<NavigationScreens>();
const profile_setup: string = 'Profile Setup';

const App = () => {
  StatusBar.setBackgroundColor(appColors.statusBar);
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="loginScreen"
          component={LoginScreen}
          options={{title: 'Login', ...app_bar_style}}
        />
        <Stack.Screen
          name="signUpScreen"
          component={SignUpScreen}
          options={{
            title: 'Sign Up',
            ...app_bar_style,
          }}
        />
        <Stack.Screen
          name="nicknameSetupScreen"
          component={NicknameSetupScreen}
          options={{
            title: profile_setup,
            headerLeft: () => <></>,
            ...app_bar_style,
            headerBackImage: () => <></>,
          }}
        />
        <Stack.Screen
          name="locationSetupScreen"
          component={LocationSetupScreen}
          options={{title: profile_setup, ...app_bar_style}}
        />
        <Stack.Screen
          name="ageSetupScreen"
          component={AgeSetupScreen}
          options={{title: profile_setup, ...app_bar_style}}
        />
        <Stack.Screen
          name="genderSetupScreen"
          component={GenderSetupScreen}
          options={{title: profile_setup, ...app_bar_style}}
        />
        <Stack.Screen
          name="sportSetupScreen"
          component={SportsSetupScreen}
          options={{title: profile_setup, ...app_bar_style}}
        />
        <Stack.Screen
          name="proficiencySetupScreen"
          component={ProficiencySetup}
          options={{title: profile_setup, ...app_bar_style}}
        />

        <Stack.Screen
          name="dashboardScreen"
          component={DashboardScreen}
          options={{title: 'Dashboard', ...app_bar_style}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;

const app_bar_style = {
  headerStyle: Styles.appBar,
  headerTitleStyle: Styles.appBarText,
  headerBackImage: () => (
    <FontAwesomeIcon icon="arrow-left" style={Styles.appBarIcon} />
  ),
};
