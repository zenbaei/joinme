echo 'create assets folder in the current project'
mkdir android/app/src/main/assets

echo 'create bundle script'
npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/

# execute command to run android to create debug apk
cd android

echo 'build debug apk'
./gradlew assembleDebug

echo 'install app'
./gradlew :app:installDebug

echo 'upload file to server'
curl -F 'sampleFile=@./app/build/outputs/apk/debug/app-debug.apk' http://zenbaei.ddns.net:8000/file/upload
