crest rest interface is used for test env on top of mongo.
crest is listening on port 3500.

To test crest is up, do Post curl:
curl -d '{ "Key" : 42 }' -H "Content-Type: application/json" http://localhost:3500/test/example

then Get:
curl --header "Content-Type: application/json" http://localhost:3500/test/example
Note: url quries must be encoded first.
