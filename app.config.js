const server_ip = 'zenbaei.ddns.net';
//const android_emulator = '10.0.2.2';
const port = '3500';
const dev_dbName = 'test';

export const rest_server_url = `http://${server_ip}:${port}/${dev_dbName}`;

export const android = {
  rest_server_url: `http://${server_ip}:${port}/${dev_dbName}`,
};

export const send_message_url = `http://${server_ip}:8000/message/send`;

export const ip_data_api =
  'https://api.ipdata.co/?api-key=70db9e3404500cbd042097a219048e0fd28f3144ac0091cde2fd3991';
