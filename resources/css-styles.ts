import {StyleSheet} from 'react-native';
import appColors from './app-colors';

const padding = 10;
const radius_rectangle = 10;
const radius_round = 25;
const border_width = 1;

const Styles = StyleSheet.create({
  grid: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    backgroundColor: appColors.background,
  },
  gridRow: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    padding: padding,
  },
  gridCol: {
    flexDirection: 'column',
  },
  card: {
    width: '95%',
    backgroundColor: appColors.surface,
    borderRadius: radius_round,
    alignSelf: 'center',
    alignItems: 'center',
    padding: padding,
    margin: 10,
    elevation: 2,
    borderColor: appColors.primary,
    borderWidth: border_width,
  },
  inputText: {
    borderColor: appColors.primary,
    borderWidth: border_width,
    borderRadius: radius_rectangle,
    width: '90%',
    margin: 5,
    backgroundColor: appColors.background,
    color: appColors.onSurface,
  },
  link: {
    elevation: 5,
  },
  linkActive: {
    color: appColors.primary,
    textDecorationLine: 'underline',
  },
  linkVisited: {
    color: appColors.primaryDark,
    textDecorationLine: 'underline',
  },
  appBar: {
    backgroundColor: appColors.appBar,
  },
  appBarText: {
    color: appColors.onSurface,
  },
  iconButton: {
    backgroundColor: appColors.surface,
    borderRadius: radius_round,
    borderColor: appColors.primary,
    borderWidth: border_width,
    padding: padding,
  },
  icon: {
    alignSelf: 'center',
    color: appColors.onSurface,
  },
  iconVisited: {
    color: appColors.background,
  },
  sliderMinVal: {
    color: appColors.onSurface,
  },
  sliderMaxVal: {
    color: appColors.appBar,
  },
  appBarIcon: {
    color: appColors.onSurface,
  },
  text: {
    color: appColors.onSurface,
  },
  button: {
    backgroundColor: appColors.background,
    padding: padding,
    alignItems: 'center',
    borderRadius: radius_rectangle,
    borderColor: appColors.primary,
    borderWidth: border_width,
    elevation: 5,
  },
  buttonText: {
    color: appColors.onSurface,
  },
  picker: {
    alignSelf: 'stretch',
    color: 'white',
    backgroundColor: appColors.background,
  },
  errorText: {color: 'red'},
  showBorder: {borderWidth: 2, borderColor: 'black', backgroundColor: 'red'},
  ageSlider: {width: 200, height: 40},
  proficiencySlider: {width: 150, height: 40},
});

export default Styles;

/*
type FunctionStyle = (value: string) => ViewStyle;

/**
 * This type is defined in react-native under StyleSheet.
 * Here I'm just adding a new Type (FunctionStyle) to allow
 * adding functions to StyleSheet.create.
 *
type NamedStyles<T> = {
  [P in keyof T]: ViewStyle | TextStyle | ImageStyle | FunctionStyle;
};

/**
 * The purpose of this interface is to allow adding a function under StyleSheet.create
 * without having typscript complaining that functions aren't defined.
 *
interface OverloadedStyleSheet {
  /**
   * Overloading StyleSheet.create's parameters type.
   * @param styles
   *
  create<T extends NamedStyles<T> | NamedStyles<any>>(
    styles: T | NamedStyles<T>,
  ): T;
}
*/
