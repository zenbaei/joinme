import User from 'src/domain/user/user';

export type NavigationScreens = {
  loginScreen: {user: User}; //undefined for no values
  signUpScreen: {user: User}; //means expecting a user object from route.params
  nicknameSetupScreen: {user: User};
  locationSetupScreen: {user: User};
  ageSetupScreen: {user: User};
  genderSetupScreen: {user: User};
  sportSetupScreen: {user: User};
  proficiencySetupScreen: {user: User};
  dashboardScreen: {user: User};
};
