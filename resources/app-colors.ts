const appColors = {
  background: '#303030',
  surface: '#424242',
  primary: '#1EB980',
  primaryDark: '#045D56',
  onSurface: 'white',
  appBar: '#212121',
  statusBar: 'black',
};

export default appColors;
