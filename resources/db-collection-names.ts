const db_collection_names = {
  users: 'users',
  countries: 'countries',
};

export default db_collection_names;
